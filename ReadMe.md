## 最简单的基于FFmpeg的AVDevice例子
### 作者介绍
+ 雷霄骅 Lei Xiaohua leixiaohua1020@126.com
+ 中国传媒大学/数字电视技术 Communication University of China / Digital TV Technology
+ https://blog.csdn.net/leixiaohua1020/article/details/18155549

### 环境
Win 10 Visual Studio Community 2015


### 项目介绍
本工程包含两个基于FFmpeg的libavdevice的例子：
+ simplest_ffmpeg_grabdesktop：屏幕录制。
+ simplest_ffmpeg_readcamera：读取摄像头。


#### 参考资料
[读取摄像头](https://blog.csdn.net/leixiaohua1020/article/details/39702113)
[屏幕录制](https://blog.csdn.net/leixiaohua1020/article/details/39706721)